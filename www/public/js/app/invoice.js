$('.chosen').chosen({
    width: "100%"
});
$('#data_date').inputmask("timestamp", {
    mask:        "y-m-d h:s",
    placeholder: "yyyy-mm-dd hh:mm",
    separator:   "-",
    alias:       "datetime",
    hourFormat:  "24",
});
$('.currency').inputmask('currency', {
    prefix: "",
    suffix: " đ",
    digits: 0,
    removeMaskOnSubmit: true
});

$("#invoice-list-date").change(function() {
    location.href = "?date=" + $("#invoice-list-date").val();
});
CKEDITOR.replace('data_note');
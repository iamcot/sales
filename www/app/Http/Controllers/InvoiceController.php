<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Client;
use App\Models\ClientType;
use App\Models\Invoice;
use App\Models\InvoiceType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;

class InvoiceController extends Controller
{
    private $invoiceModel;
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function anyIndex()
    {
        return redirect()->action('InvoiceController@anyOut');
    }

    public function anyOut()
    {
        $redirect = $this->processInout(InvoiceType::INVOICE_TYPE_EXPORT_ID, ClientType::CLIENT_TYPE_STORE_ID, ClientType::CLIENT_TYPE_WHOLESALE_ID);
        if ($redirect) {
            return redirect($redirect);
        }
        $this->view['from'] = [
            'id' => ClientType::CLIENT_TYPE_STORE_ID,
            'title' => ClientType::CLIENT_TYPE_STORE_TITLE
        ];
        $this->view['to'] = [
            'id' => ClientType::CLIENT_TYPE_WHOLESALE_ID,
            'title' => ClientType::CLIENT_TYPE_WHOLESALE_TITLE
        ];
        $this->view['title'] = InvoiceType::INVOICE_TYPE_EXPORT_TITLE;

        return view('invoice.invoice', $this->view);
    }


    public function anyIn()
    {
        $redirect = $this->processInout(InvoiceType::INVOICE_TYPE_IMPORT_ID, ClientType::CLIENT_TYPE_SUPPLIER_ID, ClientType::CLIENT_TYPE_STORE_ID);
        if ($redirect) {
            return redirect($redirect);
        }

        $this->view['from'] = [
            'id' => ClientType::CLIENT_TYPE_SUPPLIER_ID,
            'title' => ClientType::CLIENT_TYPE_SUPPLIER_TITLE
        ];
        $this->view['to'] = [
            'id' => ClientType::CLIENT_TYPE_STORE_ID,
            'title' => ClientType::CLIENT_TYPE_STORE_TITLE
        ];
        $this->view['title'] = InvoiceType::INVOICE_TYPE_IMPORT_TITLE;

        return view('invoice.invoice', $this->view);
    }

    /**
     * @param $typeId
     * @param $fromId
     * @param $toId
     * @return string|null
     */
    protected function processInout($typeId, $fromId, $toId)
    {
        $this->invoiceModel = new Invoice();
        $this->view['invoiceModel'] = $this->invoiceModel;

        $data = [];
        $this->view['data'] = $data;
        if (Input::get('save', '') == 'invoice') {
            $data = Input::get('data');
            list($result, $flashMessage, $validator) = $this->invoiceModel->saveInvoice($data);
            if ($flashMessage) {
                $this->addFlashMessage($flashMessage);
            }
            if ($result) {
                return \Illuminate\Support\Facades\Request::path();
            } elseif ($validator) {
                $this->view['validators'] = $validator;
            }
        } elseif (Input::has('invoice')) {
            $data = $this->invoiceModel->getInvoiceData(Input::get('invoice'));
        }

        if (empty($data['date'])) {
            $data['date'] = date('Y-m-d H:i');
        } else {
            $data['date'] = date('Y-m-d H:i', $data['date']);
        }

        $this->view['data'] = $data;

        $this->view['invoice_list'] = $this->invoiceModel->getInvoiceList($typeId);
        $this->view['from_list'] = Client::where('fk_client_types', $fromId)->get();
        $this->view['to_list'] = Client::where('fk_client_types', $toId)->get();
        $this->view['type'] = $typeId;

        return null;
    }


    public function postAddclient()
    {
        $data = Input::get('client');
        if (!$data) {
            $this->addFlashMessage('Vui lòng nhập data!');
            return redirect()->back();
        }
        $client = new Client();
        $clientType = $client->getClientTypeFromId($data['fk_client_types']);
        if (false === $clientType) {
            $this->addFlashMessage('Data không phù hợp!');
            return redirect()->back();
        }
        $clientExists = $client->isClientExist($data['fk_client_types'], $data['name'], $data['phone']);
        if ($clientExists) {
            $this->addFlashMessage($clientType . ' này đã tồn tại!');
            return redirect()->back();
        }

        $newClient = $client->create($data);
        if ($newClient) {
            $this->addFlashMessage('Thêm ' . $clientType . ' ' . $data['name'] . ' thành công');
            return redirect()->back();
        }

        $this->addFlashMessage('Không thể tạo mới, vui lòng thử lại.');
        return redirect()->back();
    }
}


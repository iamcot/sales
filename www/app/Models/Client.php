<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $table = 'clients';

    protected $primaryKey = 'id_clients';

    protected $fillable = ['fk_client_types', 'name', 'phone', 'address'];

    protected $dates = ['deleted_at'];

    public function getClientTypeFromId($id)
    {
        $type = ClientType::find($id);
        if ($type) {
            return $type->name;
        }
        return false;
    }

    public function isClientExist($type, $name, $phone)
    {
        $count = Client::where('fk_client_types', $type)
            ->where('name', $name)
            ->where('phone', $phone)
            ->count();
        if ($count > 0) {
            return true;
        }
        return false;
    }
}
<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientType extends Model
{
    protected $table = 'client_types';

    protected $primaryKey = 'id_client_types';

    const CLIENT_TYPE_STORE_ID = 1;
    const CLIENT_TYPE_SUPPLIER_ID = 2;
    const CLIENT_TYPE_WHOLESALE_ID = 3;

    const CLIENT_TYPE_STORE_TITLE = 'Kho';
    const CLIENT_TYPE_SUPPLIER_TITLE = 'Nhà cung cấp';
    const CLIENT_TYPE_WHOLESALE_TITLE = 'Khách sỉ';
}
<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceType extends Model
{
    protected $table = 'invoice_types';

    protected $primaryKey = 'id_invoice_types';

    const INVOICE_TYPE_IMPORT_ID = 1;
    const INVOICE_TYPE_EXPORT_ID = 2;
    const INVOICE_TYPE_EXPORT_TITLE = 'Xuất hàng';
    const INVOICE_TYPE_IMPORT_TITLE = 'Nhập hàng';
}
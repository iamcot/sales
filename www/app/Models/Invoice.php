<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class Invoice extends Model
{
    use SoftDeletes;

    const PP = 20;

    protected $table = 'invoices';

    protected $primaryKey = 'id_invoices';

    protected $fillable = ['fk_invoice_types', 'date', 'fk_clients_from', 'fk_clients_to', 'amount', 'value', 'paid', 'note', 'fk_users', 'id_invoices'];

    protected $dates = ['deleted_at'];

    public function to()
    {
        return $this->belongsTo('App\Models\Client', 'fk_clients_to', 'id_clients');
    }
    public function from()
    {
        return $this->belongsTo('App\Models\Client', 'fk_clients_from', 'id_clients');
    }

    public function getFormValidateRule()
    {
        return [
            'fk_clients_from' => 'required|numeric|min:1',
            'fk_clients_to' => 'required|numeric|min:1',
            'date' => 'required|numeric',
            'amount' => 'required|numeric|min:1',
            'value' => 'required',
        ];
    }

    public function getFormValidateMessage()
    {
        return [
            'fk_clients_from.min' => 'Hãy chọn một',
            'fk_clients_to.min' => 'Hãy chọn một',
            'value.required' => 'Hãy nhập giá',
            'date.required' => 'Hãy nhập ngày giờ',
            'date.numeric' => 'Hãy nhập ngày giờ hợp lệ',
            'amount.required' => 'Hãy nhập số lượng',
            'amount.min' => 'Slg > 0',
        ];
    }

    public function saveInvoice(&$data)
    {
        $data['date'] = strtotime($data['date']);
        $validator = Validator::make($data, $this->getFormValidateRule(), $this->getFormValidateMessage());
        if ($validator->fails()) {
            return [false, null, $validator->messages()];
        } else {
            $data['fk_users'] = Auth::user()->id_users;
            if (Input::has('invoice')) {
                $update = $this->find(Input::get('invoice'))
                ->update($data);
                if ($update) {
                    return [true, 'Cập nhật đơn hàng thành công', null];
                } else {
                    return [false, 'Cập nhật đơn thất bại, xin hãy thử lại', null];
                }
            } else {
                $invoice = $this->create($data);
                if ($invoice) {
                    $this->updateCode($invoice);
                    return [true, 'Tạo mới đơn hàng thành công', null];
                } else {
                    return [false, 'Tạo mới đơn thất bại, xin hãy thử lại', null];
                }
            }

        }
    }

    protected function updateCode($invoice)
    {
        $invoice->code = $invoice->id_invoices;
        $invoice->save();
    }

    public function getInvoiceList($type)
    {
        $data = $this->with('to')
            ->with('from')
            ->where('fk_invoice_types', $type);
        if (Input::has('date')) {
            $dateFrom = strtotime(Input::get('date') . ' 00:00:00');
            $dateTo = strtotime(Input::get('date') . ' 24:00:00');
            $data = $data->whereBetween('date', [$dateFrom, $dateTo]);
        }
        $data = $data->orderBy('date', 'desc')
            ->paginate(self::PP);
        return $data;
    }

    public function buildInvoiceUrl($id)
    {
        $params = ['invoice=' . $id];
        if (Input::has('page')) {
            $params[] = 'page=' . Input::get('page');
        }
        if (Input::has('date')) {
            $params[] = 'date=' . Input::get('date');
        }

        return '?' . implode('&', $params);
    }

    public function getInvoiceData($id)
    {
        $invoice = $this->find($id);
        if ($invoice) {
            $data = $invoice->toArray();
            return $data;
        }
        return [];
    }
}
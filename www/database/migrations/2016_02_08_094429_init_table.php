<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_types', function (Blueprint $table) {
            $table->increments('id_client_types');
            $table->string('name', 100);
            $table->string('code', 50)->unique();
        });
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id_clients');
            $table->integer('fk_client_types')->unsigned();
            $table->string('name', 100);
            $table->string('phone', 20);
            $table->string('address', 100);
            $table->timestamps();
            $table->softDeletes();
            $table->index('name');
            $table->index('phone');
            $table->index('fk_client_types');
            $table->foreign('fk_client_types')->references('id_client_types')->on('client_types');
        });
        Schema::create('invoice_types', function (Blueprint $table) {
            $table->increments('id_invoice_types');
            $table->string('name', 100);
            $table->string('code', 50)->unique();
        });
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id_invoices');
            $table->string('code', 50)->unique();
            $table->bigInteger('date');
            $table->integer('fk_clients_from')->unsigned();
            $table->integer('fk_clients_to')->unsigned();
            $table->integer('fk_invoice_types')->unsigned();
            $table->integer('fk_users')->unsigned();
            $table->bigInteger('value', 0);
            $table->bigInteger('paid', 0);
            $table->integer('amount', 0);
            $table->text('note');
            $table->timestamps();
            $table->softDeletes();
            $table->index('date');
            $table->foreign('fk_clients_from')->references('id_clients')->on('clients');
            $table->foreign('fk_clients_to')->references('id_clients')->on('clients');
            $table->foreign('fk_invoice_types')->references('id_invoice_types')->on('invoice_types');
            $table->foreign('fk_users')->references('id_users')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invoices');
        Schema::drop('clients');
        Schema::drop('client_types');
        Schema::drop('invoice_types');
    }
}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@sales.com',
            'password' => bcrypt('sales123'),
            'role' => 'admin'
        ]);

        DB::table('client_types')->insert([
            'name' => 'Kho',
            'code' => 'warehouse'
        ]);
        DB::table('client_types')->insert([
            'name' => 'Nhà cung cấp',
            'code' => 'supplier'
        ]);
        DB::table('client_types')->insert([
            'name' => 'Khách sỉ',
            'code' => 'wholesale'
        ]);
        DB::table('client_types')->insert([
            'name' => 'Khách lẻ',
            'code' => 'retail'
        ]);
        DB::table('invoice_types')->insert([
            'name' => 'Nhập hàng',
            'code' => 'import'
        ]);
        DB::table('invoice_types')->insert([
            'name' => 'Xuất hàng',
            'code' => 'export'
        ]);
        DB::table('invoice_types')->insert([
            'name' => 'Tiền vào',
            'code' => 'income'
        ]);
        DB::table('invoice_types')->insert([
            'name' => 'Tiền ra',
            'code' => 'outcome'
        ]);
        DB::table('invoice_types')->insert([
            'name' => 'Chuyển Tiền Nội bộ',
            'code' => 'money-transfer'
        ]);
        DB::table('invoice_types')->insert([
            'name' => 'Chuyển Hàng Nội bộ',
            'code' => 'product-transfer'
        ]);

    }
}

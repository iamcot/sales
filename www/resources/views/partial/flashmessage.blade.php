@if (\Illuminate\Support\Facades\Session::has('message'))
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @foreach(\Illuminate\Support\Facades\Session::get('message') as $message)
                    <div class="alert alert-warning">
                        {{ $message }}
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
<div class="modal fade" id="add{{ $client['id'] }}modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm {{ $client['title'] }}</h4>
            </div>
            <form method="POST" action="/invoice/addclient">
            <div class="modal-body">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                    <input type="hidden" name="client[fk_client_types]" value="{{ $client['id'] }}">
                    <div class="form-group col-md-6">
                        <label for="">Tên {{ $client['title'] }}</label>
                        <input type="text" class="form-control" placeholder="Tên {{ $client['title'] }}" name="client[name]">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="">SĐT</label>
                        <input type="text" class="form-control" placeholder="SDT"  name="client[phone]">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="">Địa chỉ</label>
                        <input type="text" class="form-control" placeholder="Địa chỉ" name="client[address]">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="submit" class="btn btn-primary" name="save" value="client">Lưu</button>
            </div>
            </form>
        </div>
    </div>
</div>
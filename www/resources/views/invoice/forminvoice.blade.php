<form method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="data[fk_invoice_types]" value="{{ $type }}">
    <div class="row">
        <div class="form-group col-md-6 {{ isset($validators) && $validators->has('fk_clients_from') ? 'has-error' : '' }}">
            <label for="exampleInputEmail1">{{ $from['title'] }} <a title="Thêm mới" data-toggle="modal"
                                                                    data-target="#add{{ $from['id'] }}modal">
                    <i class="fa fa-plus-square"></i></a>
            </label>
            <select class="chosen" name="data[fk_clients_from]" id="data_from">
                <option value="0">Chọn</option>
                @foreach($from_list as $option)
                    <option {{ isset($data['fk_clients_from']) && $data['fk_clients_from'] == $option->id_clients ? 'selected' : '' }}
                            value="{{ $option->id_clients }}">{{ $option->name . ' ('. $option->phone . ')' }}</option>
                @endforeach
            </select>
            @if (isset($validators) && $validators->has('fk_clients_from'))
                <span id="data_from" class="help-block">{{ $validators->first('fk_clients_from') }}</span>
            @endif
        </div>
        <div class="form-group col-md-6 {{ isset($validators) && $validators->has('fk_clients_to') ? 'has-error' : '' }}">
            <label for="exampleInputPassword1">{{ $to['title'] }} <a href="" title="Thêm mới" data-toggle="modal"
                                                                     data-target="#add{{ $to['id'] }}modal">
                    <i class="fa fa-plus-square"></i></a>
            </label>
            <select class="chosen" name="data[fk_clients_to]" id="data_to">
                <option value="0">Chọn</option>
                @foreach($to_list as $option)
                    <option {{ isset($data['fk_clients_to']) && $data['fk_clients_to'] == $option->id_clients ? 'selected' : '' }}
                            value="{{ $option->id_clients }}">{{ $option->name . ' ('. $option->phone . ')' }}</option>
                @endforeach
            </select>
            @if (isset($validators) && $validators->has('fk_clients_to'))
                <span id="data_to" class="help-block">{{ $validators->first('fk_clients_to') }}</span>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="form-group col-md-4 {{ isset($validators) && $validators->has('date') ? 'has-error' : '' }}">
            <label for="">Thời gian</label>
            <input type="text" class="form-control" placeholder="Thời gian"
                   name="data[date]" id="data_date" value="{{ isset($data['date']) ? $data['date'] : '' }}">
            @if (isset($validators) && $validators->has('date'))
                <span id="data_from" class="help-block">{{ $validators->first('date') }}</span>
            @endif
        </div>
        <div class="form-group col-md-2 {{ isset($validators) && $validators->has('amount') ? 'has-error' : '' }}">
            <label for="">Slg</label>
            <input type="number" class="form-control" placeholder="Slg" name="data[amount]"
                   value="{{ isset($data['amount']) ? $data['amount'] : 1 }}">
            @if (isset($validators) && $validators->has('amount'))
                <span id="data_from" class="help-block">{{ $validators->first('amount') }}</span>
            @endif
        </div>
        <div class="form-group col-md-3 {{ isset($validators) && $validators->has('value') ? 'has-error' : '' }}">
            <label for="">Thành tiền</label>
            <input type="text" class="form-control currency" placeholder="Thành tiền" id="date_value"
                   name="data[value]" value="{{ isset($data['value']) ? $data['value'] : '' }}">
            @if (isset($validators) && $validators->has('value'))
                <span id="data_from" class="help-block">{{ $validators->first('value') }}</span>
            @endif
        </div>
        <div class="form-group col-md-3">
            <label for="">Thanh toán</label>
            <input type="text" class="form-control currency" placeholder="Thanh toán" id="date_paid"
                   name="data[paid]" value="{{ isset($data['paid']) ? $data['paid'] : '' }}">
        </div>
    </div>
    <div class="row">
        <div class="form-group col-xs-12">
            <label for="">Ghi chú</label>
            <textarea class="form-control" name="data[note]" id="data_note">{{ isset($data['note']) ? $data['note'] : '' }}</textarea>
        </div>
        <div class="col-xs-12">
            <button class="btn btn-success" name="save" value="invoice"><i class="fa fa-save"></i> Lưu đơn hàng</button>
        </div>
    </div>
</form>
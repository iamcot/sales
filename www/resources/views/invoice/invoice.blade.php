@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">Quản lý {{ $title }}
                        @if (\Illuminate\Support\Facades\Input::has('invoice'))
                            <a class="btn btn-info btn-xs" href="/{{ \Illuminate\Support\Facades\Request::path() }}">Tạo
                                đơn hàng mới</a>
                            <p class="pull-right">Chỉnh sửa đơn hàng
                                #{{ \Illuminate\Support\Facades\Input::get('invoice') }}</p>
                        @endif
                    </div>
                    <div class="panel-body">
                        @include('invoice.forminvoice', array(
                            'from' => $from,
                            'to' => $to,
                            'from_list' => $from_list,
                            'to_list' => $to_list,
                            'data' => $data,
                            'type' => $type
                        ))
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-5">
                                <i class="fa fa-list"></i> Đơn hàng
                            </div>
                            <div class="col-md-7">
                                <input type="date" class="form-control" id="invoice-list-date"
                                       value="{{ \Illuminate\Support\Facades\Input::has('date') ? \Illuminate\Support\Facades\Input::get('date') : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body no-padding">
                        @include('invoice.invoicelist', array(
                            'type' => $type,
                            'invoice_list' => $invoice_list,
                            'invoiceModel' => $invoiceModel
                        ))
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('invoice.modaladdclient', array('client' => $from))
    @include('invoice.modaladdclient', array('client' => $to))
@endsection
@section('js')
    <script src="{{ URL::asset('ckeditor/ckeditor.js') }}"></script>
    <script src="{{ URL::asset('js/app/invoice.js') }}"></script>
@endsection

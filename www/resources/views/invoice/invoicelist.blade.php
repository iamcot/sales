<table class="table table-hover">
    <thead>
    <th>Mã</th>
    <th>
        @if ($type == \App\Models\InvoiceType::INVOICE_TYPE_EXPORT_ID)
            Khách sỉ
        @else
            Nhà cung cấp
        @endif
    </th>
    <th>Giá trị</th>
    <th>
        Thời gian
    </th>
    </thead>
    <tbody>
    @foreach($invoice_list as $invoice)
        <tr>
            <td>
                <a href="{{ $invoiceModel->buildInvoiceUrl($invoice->id_invoices) }}">
                    {{ $invoice->code }}
                </a>
            </td>
            <td>
                <a href="{{ $invoiceModel->buildInvoiceUrl($invoice->id_invoices) }}">
                    @if ($type == \App\Models\InvoiceType::INVOICE_TYPE_EXPORT_ID)
                        {{ $invoice->to->name }}
                    @else
                        {{ $invoice->from->name }}
                    @endif
                </a>
            </td>
            <td>
                <div class="pull-right">{{ number_format($invoice->value, 0, '.', ',') }}</div>
            </td>
            <td >
                <a class="text-success" href="?date={{ date('Y-m-d', $invoice->date) }}">
                    {{ date('d/m H:i', $invoice->date) }}
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="col-xs-12">
    @if (\Illuminate\Support\Facades\Input::has('date'))
        {!! $invoice_list->appends(['date' => \Illuminate\Support\Facades\Input::get('date')])->render() !!}
    @else
        {{ $invoice_list->links() }}
    @endif
</div>